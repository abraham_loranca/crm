<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translatable extends Model
{
    use Translatable;
    protected $translatable = ['title', 'body'];
}
