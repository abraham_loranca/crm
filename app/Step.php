<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Seld\PharUtils\Timestamps;
use TCG\Voyager\Http\Controllers\ContentTypes\Timestamp;

class Step extends Model
{
   public $timestamps = false; 
}
